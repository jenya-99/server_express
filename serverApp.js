const express = require("express");
const app = express();

//подключение всех статических файлов в папке /static
// В браузере - без папки /static
app.use(express.static(__dirname + "/static"));
let port = process.env.PORT;//for Heroku

app.use("/public", function(request, response) {     
  // отправляем содержимое файла:
  response.sendFile(__dirname + "/index.html");
  console.log(request);
});

let artists = {
  id: 1,
  name: 'Scorpions'
};
app.use("/artists", function(request, response) {     
  // отправляем объект:
  response.send(artists);
});
app.use("/", function(request, response){
//получим параметры из адресной строки:
  let name = request.query.name;
  let lastName = request.query.lastName;
  if (!request.query.name && !request.query.lastName)
  {response.send("<h1>Главная страница</h1>")} else
  response.send("<h1>Главная страница</h1><p>name = " + 
        name +"</p><p>lastName = " + lastName + "</p>");   
});

// начинаем прослушивать подключения на 3001 порту
app.listen(port || 3000, function() {
  console.log('ServerApp is started!!');
});